exports.up = function (knex) {
  return knex.schema.createTable("products", function (t) {
    t.increments();

    t.string("name").notNullable();
    t.string("sku").notNullable().unique();
    t.string("image");
    t.decimal("price", 10, 2).notNullable();
    t.integer("stock").notNullable();
    t.text("description");

    t.timestamp("createdAt").defaultTo(knex.fn.now());
    t.timestamp("updatedAt").defaultTo(knex.fn.now());
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("products");
};
