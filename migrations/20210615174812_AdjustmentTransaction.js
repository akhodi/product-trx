exports.up = function (knex) {
  return knex.schema.createTable("adjustment_transactions", function (t) {
    t.increments();

    t.integer("product_id").unsigned();
    t.foreign("product_id").references("Products");
    t.integer("qty").notNullable();
    t.decimal("amount", 10, 2).notNullable();

    t.timestamp("createdAt").defaultTo(knex.fn.now());
    t.timestamp("updatedAt").defaultTo(knex.fn.now());
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("adjustment_transactions");
};
