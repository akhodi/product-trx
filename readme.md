#### Quick Setup / Re-Setup:
```
knex migrate:latest
```


#### Development Environment:
```bash
npm install
```


#### Run Web Server
```
npm start
```
