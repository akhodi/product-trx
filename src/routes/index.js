"use strict";

const Products = require("./products");
const AdjustmentTransactions = require("./adjustment_transactions");

module.exports = Products.concat(AdjustmentTransactions);
