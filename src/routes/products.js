"use strict";

const Joi = require("joi");

const Product = require("../app/controllers/product");

module.exports = [
  {
    method: "GET",
    path: "/products",
    handler: Product.index,
  },
  {
    method: "GET",
    path: "/products/{id}",
    handler: Product.show,
    options: {
      validate: {
        params: Joi.object({ id: Joi.number() }),
      },
    },
  },
  {
    method: "POST",
    path: "/products",
    handler: Product.create,
  },
  {
    method: "PUT",
    path: "/products/{id}",
    handler: Product.update,
  },
  {
    method: "DELETE",
    path: "/products/{id}",
    handler: Product.delete,
  },
  {
    method: "POST",
    path: "/products/upload",
    handler: Product.upload,
    options: {
      payload: {
        output: "stream",
      },
    },
  },
  {
    method: "GET",
    path: "/products/elevenia",
    handler: Product.elevenia,
  },
  {
    method: "GET",
    path: "/products/pagination",
    handler: Product.pagination,
    options: {
      validate: {
        query: Joi.object({
          limit: Joi.number().integer(),
          page: Joi.number().integer(),
          pagination: Joi.boolean(),
        }),
      },
    },
  },
];
