"use strict";

const Joi = require("joi");

const AdjustmentTransaction = require("../app/controllers/adjustment_transaction");

module.exports = [
  {
    method: "GET",
    path: "/adjustment_transactions",
    handler: AdjustmentTransaction.index,
  },
  {
    method: "GET",
    path: "/adjustment_transactions/{id}",
    handler: AdjustmentTransaction.show,
    options: {
      validate: {
        params: Joi.object({ id: Joi.number() }),
      },
    },
  },
  {
    method: "POST",
    path: "/adjustment_transactions",
    handler: AdjustmentTransaction.create,
  },
  {
    method: "PUT",
    path: "/adjustment_transactions/{id}",
    handler: AdjustmentTransaction.update,
  },
  {
    method: "DELETE",
    path: "/adjustment_transactions/{id}",
    handler: AdjustmentTransaction.delete,
  },
  {
    method: "GET",
    path: "/adjustment_transactions/pagination",
    handler: AdjustmentTransaction.pagination,
    options: {
      validate: {
        query: Joi.object({
          limit: Joi.number().integer(),
          page: Joi.number().integer(),
          pagination: Joi.boolean(),
        }),
      },
    },
  },
];
