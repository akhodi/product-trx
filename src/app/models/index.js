"use strict";

const Fs = require("fs");
const Path = require("path");
const Sequelize = require("sequelize");
const Config = require("../../../config");
const dbConfig = Config[Config.env].db;

const sequelize = new Sequelize(
  dbConfig.database,
  dbConfig.user,
  dbConfig.password,
  dbConfig
);
const db = {};

Fs.readdirSync(__dirname)
  .filter(file => file.indexOf(".") !== 0 && file !== "index.js")
  .forEach(file => {
    const model = require(Path.join(__dirname, file))(sequelize, Sequelize.DataTypes)
    db[model.name] = model;
  });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
