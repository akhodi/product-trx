"use strict";

module.exports = (sequelize, DataTypes) => {
  const products = sequelize.define("products", {
    sku: DataTypes.STRING,
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    image: DataTypes.STRING,
    price: DataTypes.DECIMAL(10, 2),
    stock: DataTypes.INTEGER
  });

  return products;
};
