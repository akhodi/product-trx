"use strict";

module.exports = (sequelize, DataTypes) => {
  const adjustment_transactions = sequelize.define("adjustment_transactions", {
    product_id: DataTypes.BIGINT,
    qty: DataTypes.INTEGER,
    amount: DataTypes.DECIMAL(10, 2)
  });

  return adjustment_transactions;
};
