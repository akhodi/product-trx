"use strict";

const { adjustment_transactions, products } = require("../models");

module.exports = {
  index: async () => {
    const result = await adjustment_transactions
      .findAll()
      .then((res) => {
        return { data: res };
      })
      .catch((err) => {
        return { message: err.errors[0].message };
      });

    return result;
  },

  show: async (request) => {
    const { id } = request.params;
    const result = await adjustment_transactions
      .findOne({
        where: { id },
      })
      .then((res) => {
        return { data: res || {} };
      })
      .catch(() => {
        return { message: "Record not found" };
      });

    return result;
  },

  create: async (request) => {
    const { product_id, qty } = request.payload;

    const result = await products
      .findOne({
        where: { id: product_id },
      })
      .then(async (res) => {
        if (res) {
          // validate if out of stock
          if (parseInt(res.stock) == 0) {
            return { message: "Out of stock" };
          }

          // validate if not enough stock
          if (parseInt(res.stock) < parseInt(qty)) {
            return { message: "Not enough stock" };
          }

          const saved = await adjustment_transactions
            .create({
              product_id,
              qty,
              amount: parseInt(res.price) * parseInt(qty),
            })
            .then((trx) => {
              // update stock
              products.update(
                { stock: parseInt(res.stock) - parseInt(qty) },
                { where: { id: product_id } }
              );

              return { data: trx };
            })
            .catch((err) => {
              return { message: err.errors[0].message };
            });

          return saved;
        }

        return { message: "Product not found" };
      })
      .catch(() => {
        return { message: "Product not found" };
      });

    return result;
  },

  update: async (request) => {
    const { id } = request.params;
    const { product_id, qty } = request.payload;

    const options = { where: { id } };

    const result = await products
      .findOne({
        where: { id: product_id },
      })
      .then(async (res) => {
        if (res) {
          // validate if out of stock
          if (parseInt(res.stock) == 0) {
            return { message: "Out of stock" };
          }

          // validate if not enough stock
          if (parseInt(res.stock) < parseInt(qty)) {
            return { message: "Not enough stock" };
          }

          const values = {
            product_id,
            qty,
            amount: parseInt(res.price) * parseInt(qty),
          };
          const updated = await adjustment_transactions
            .update(values, options)
            .then((trx) => {
              // update stock
              products.update(
                { stock: parseInt(res.stock) - parseInt(qty) },
                { where: { id: product_id } }
              );

              return {
                data: {
                  id,
                  product_id,
                  qty,
                },
              };
            })
            .catch((err) => {
              return { message: err.errors[0].message };
            });

          return updated;
        }

        return { message: "Product not found" };
      })
      .catch(() => {
        return { message: "Product not found" };
      });

    return result;
  },

  delete: async (request) => {
    const { id } = request.params;

    const result = await adjustment_transactions
      .findOne({
        where: { id },
      })
      .then(async (res) => {
        if (res) {
          await adjustment_transactions.destroy({ where: { id } });
          return { message: "Record has been deleted" };
        }

        return { message: "Record not found" };
      })
      .catch(() => {
        return { message: "Record not found" };
      });

    return result;
  },
  pagination: async (_, h) => {
    const result = await adjustment_transactions
      .findAndCountAll({
        limit: _.query.limit,
        offset: (parseInt(_.query.page) - 1) * parseInt(_.query.limit),
      })
      .then((res) => {
        return res;
      });

    const pagination = h.paginate(result.rows, result.count);
    return pagination;
  },
};
