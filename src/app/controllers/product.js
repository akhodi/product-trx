"use strict";

const fs = require("fs");
const mkdirp = require("mkdirp");
const http = require("http");

const { products, adjustment_transactions } = require("../models");

const createDir = (dir) => {
  if (!fs.existsSync(dir)) {
    mkdirp(dir, (err) => {
      if (err) {
        console.log(`Error Creating Directory ${dir}`);
      } else {
        console.log(`Creating Directory ${dir}`);
      }
    });
  }
};

const handleFileUpload = (file) => {
  const dir = "./uploads";
  createDir(dir);

  return new Promise((resolve, reject) => {
    const ext =
      file.hapi.filename.split(".")[file.hapi.filename.split(".").length - 1];
    const filename = `${new Date().getTime()}.${ext}`;
    const data = file._data;

    fs.writeFile(`${dir}/${filename}`, data, (err) => {
      if (err) {
        reject(err);
      }
      resolve({
        message: "Upload successfully!",
        imagePath: `${dir}/${filename}`,
      });
    });
  });
};

module.exports = {
  index: async () => {
    const result = await products
      .findAll()
      .then((res) => {
        return { data: res };
      })
      .catch((err) => {
        return { message: err.errors[0].message };
      });

    return result;
  },

  show: async (request) => {
    const { id } = request.params;
    const result = await products
      .findOne({
        where: { id },
      })
      .then((res) => {
        return { data: res || {} };
      })
      .catch(() => {
        return { message: "Record not found" };
      });

    return result;
  },

  create: async (request) => {
    const { sku, name, description, image, price, stock } = request.payload;
    const result = await products
      .create({
        sku,
        name,
        description,
        price,
        stock,
      })
      .then((res) => {
        return { data: res };
      })
      .catch((err) => {
        return { message: err.errors[0].message };
      });

    return result;
  },

  update: async (request) => {
    const { id } = request.params;
    const { sku, name, description, price, stock } = request.payload;

    const values = { sku, name, description, price, stock };
    const options = { where: { id } };

    const result = await products
      .update(values, options)
      .then((res) => {
        return {
          data: {
            id,
            sku,
            name,
            description,
            price,
            stock,
          },
        };
      })
      .catch((err) => {
        return { message: err.errors[0].message };
      });

    return result;
  },

  delete: async (request) => {
    const { id } = request.params;

    const result = await products
      .findOne({
        where: { id },
      })
      .then(async (res) => {
        if (res) {
          await products.destroy({ where: { id } });
          await adjustment_transactions.destroy({ where: { product_id: id } });
          return { message: "Record has been deleted" };
        }

        return { message: "Record not found" };
      })
      .catch(() => {
        return { message: "Record not found" };
      });

    return result;
  },
  upload: async (request) => {
    const { payload } = request;

    const response = handleFileUpload(payload.file);
    return response;
  },
  elevenia: async () => {
    const options = {
      method: "GET",
      port: 80,
      headers: { openapikey: process.env.ELEVENIA_API_KEY },
      host: "api.elevenia.co.id",
      path: "/rest/prodservices/product/listing?page=1",
    };

    return new Promise((resolve, reject) => {
      http
        .request(options, (res) => {
          let body = "";
          res.on("data", (chunk) => (body += chunk));
          res.on("end", () => resolve(body));
        })
        .on("error", reject)
        .end();
    });
  },
  pagination: async (_, h) => {
    const result = await products
      .findAndCountAll({
        limit: _.query.limit,
        offset: (parseInt(_.query.page) - 1) * parseInt(_.query.limit),
      })
      .then((res) => {
        return res;
      });

    const pagination = h.paginate(result.rows, result.count);
    return pagination;
  },
};
