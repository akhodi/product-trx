"use strict";

const Hapi = require("@hapi/hapi");

const Config = require("../config");
const Routes = require("./routes");
const Models = require("./app/models");

const init = async () => {
  const server = new Hapi.Server({ port: Config.port });

  const options = {
    query: {
      page: {
        name: "page",
        default: 1,
      },
      limit: {
        name: "limit",
        default: 25,
      },
      pagination: {
        name: "pagination",
        default: true,
        active: true,
      },
    },
    meta: {
      page: {
        active: true,
      },
      self: {
        active: false,
        name: "self",
      },
      first: {
        active: false,
        name: "first",
      },
      last: {
        active: false,
        name: "last",
      },
    },
    routes: {
      include: ["/products/pagination", "/adjustment_transactions/pagination"],
      exclude: [],
    },
  };

  server.route(Routes);

  await Models.sequelize.sync();

  await server.register({
    plugin: require("hapi-pagination"),
    options: options,
  });
  await server.start();
  console.log(`Server running at: ${server.info.uri}`);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
